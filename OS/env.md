# environment
```shell
cd tools
sudo apt install gcc
sudo apt install g++
sudo apt install make
sudo apt install x11-dev
tar -xzvf bochs-2.7.tar.gz
cd bochs-2.7
./configure --with-nogui
make
make install
```
