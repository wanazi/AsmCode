import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        File file=new File(args[0]);
        File outfile=new File(args[1]);
        byte[] data=Files.readAllBytes(file.toPath());
        
        byte end0=0x55;
        byte end1= (byte)0xaa;
        int foundIndex=-1;
        for(int i=0;i<data.length;i++) {
        	if(i%16==0) {
        		System.out.println("");
        	}
        	String hex=Integer.toHexString(data[i]&0xff);
        	String s=String.format("%2s", hex);
        	System.out.printf("|%s|", s);
        	if(i>0) {
        		if(data[i]==end1 && data[i-1]==end0) {
        			foundIndex=i;
        			break;
        		}
        	}
        }
        System.out.println("");
        System.out.println(foundIndex);
        int startIndex=foundIndex-511;
        byte ddd[]=new byte[512];
        System.arraycopy(data, startIndex, ddd, 0, 512);
        
        for(int i=0;i<ddd.length;i++) {
        	if(i%16==0) {
        		System.out.println("");
        	}
        	String hex=Integer.toHexString(ddd[i]&0xff);
        	String s=String.format("%2s", hex);
        	System.out.printf("|%s|", s);
        }
        
        Files.write(outfile.toPath(), ddd, StandardOpenOption.CREATE);
    }
}