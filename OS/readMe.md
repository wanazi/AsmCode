### 第一次写一个引导程序，还是记下来吧

## 1 编写引导程序代码

文件名称boot.s

```assembly
.code16
.section .data
.section .text
.global _start
.equ BOOTSEG,0x07c0
_start:
ljmp $BOOTSEG,$go
go:
    movw %cs,%ax
    movw %ax,%ds
    movw %ax,%es
    # movw $17,%di
    # movb $0xa,%bl
    # movw $msg1,%cx
    # movb %bl,(%cx,%di,1)
    movw $19,%cx
    movw $0x1004,%dx
    movw $0x000c,%bx
    movw $msg1,%bp
    movw $0x1301,%ax
    int $0x10
loop1:
    jmp loop1
msg1:
    .ascii "Loading system..."
    .byte 13,10
    .org 510
    .word 0xAA55
```

这段代码的作用就是在屏幕上显示Loading system...,然后进入死循环

然后编译
```shell
as boot.s -o build/boot.o   #32bit
as --32 boot.s -o build/boot.o #64bit
```

链接

```shell
ld -Ttext 0x00 build/boot.o -o build/boot #32bit 
ld -m elf_i386 -Ttext 0x00 build/boot.o -o build/boot #64bit
```
去掉boot文件的elf文件头，以及符号信息，使用java代码
```shell
javac App.java -d build
cd build
java App boot boot.bin 
```
执行代码后得到build/boot.bin文件

创建启动软盘文件

```shell
cd build
bximage
========================================================================
                                bximage
  Disk Image Creation / Conversion / Resize and Commit Tool for Bochs
         $Id: bximage.cc 13069 2017-02-12 16:51:52Z vruppert $
========================================================================

1. Create new floppy or hard disk image
2. Convert hard disk image to other format (mode)
3. Resize hard disk image
4. Commit 'undoable' redolog to base image
5. Disk image info

0. Quit

Please choose one [0] 1

Create image

Do you want to create a floppy disk image or a hard disk image?
Please type hd or fd. [hd] fd

Choose the size of floppy disk image to create.
Please type 160k, 180k, 320k, 360k, 720k, 1.2M, 1.44M, 1.68M, 1.72M, or 2.88M.
 [1.44M] 1.44M

What should be the name of the image?
[a.img] a.img

Creating floppy image 'a.img' with 2880 sectors

The following line should appear in your bochsrc:
  floppya: image="a.img", status=inserted
```

将可执行文件boot.bin写入软盘

```shell
dd if=boot.bin of=a.img bs=512 count=1 conv=notrunc
```

编写bochsrc配置文件

```ini
megs:32
romimage:file=/home/wanazi/Documents/bochs-2.6.9/bios/BIOS-bochs-latest
vgaromimage:file=/home/wanazi/Documents/bochs-2.6.9/bios/VGABIOS-lgpl-latest
floppya:1_44=a.img,status=inserted
boot:floppy
log:bochsout.text
mouse:enabled=0
keyboard:type=mf, serial_delay=200, paste_delay=100000
```

启动bochs

```shell
wanazi@wanazi-PC:/disk_data/code/git/AsmCode/OS$ bochs -f bochsrc
========================================================================
                       Bochs x86 Emulator 2.6.9
               Built from SVN snapshot on April 9, 2017
                  Compiled on Nov 28 2017 at 20:28:07
========================================================================
00000000000i[      ] BXSHARE not set. using compile time default '/usr/local/share/bochs'
00000000000i[      ] reading configuration from bochsrc
------------------------------
Bochs Configuration: Main Menu
------------------------------

This is the Bochs Configuration Interface, where you can describe the
machine that you want to simulate.  Bochs has already searched for a
configuration file (typically called bochsrc.txt) and loaded it if it
could be found.  When you are satisfied with the configuration, go
ahead and start the simulation.

You can also start bochs with the -q option to skip these menus.

1. Restore factory default configuration
2. Read options from...
3. Edit options
4. Save options to...
5. Restore the Bochs state from...
6. Begin simulation
7. Quit now

Please choose one: [6] 6
00000000000i[      ] installing x module as the Bochs GUI
00000000000i[      ] using log file bochsout.text
Next at t=0
(0) [0x0000fffffff0] f000:fff0 (unk. ctxt): jmpf 0xf000:e05b  ; ea5be000f0
<bochs:1> b 0x7c00
<bochs:2> c
(0) Breakpoint 1, 0x00007c00 in ?? ()
Next at t=14040237
(0) [0x000000007c00] 0000:7c00 (unk. ctxt): jmpf 0x07c0:0005  ; ea0500c007
<bochs:3> n
Next at t=14040238
(0) [0x000000007c05] 07c0:0005 (unk. ctxt): mov ax, cs  ; 8cc8
<bochs:4> n
Next at t=14040239
(0) [0x000000007c07] 07c0:0007 (unk. ctxt): mov ds, ax  ; 8ed8
<bochs:5> n
Next at t=14040240
(0) [0x000000007c09] 07c0:0009 (unk. ctxt): mov es, ax  ; 8ec0
<bochs:6> n
Next at t=14040241
(0) [0x000000007c0b] 07c0:000b (unk. ctxt): mov cx, 0x0013  ; b91300
<bochs:7> n
Next at t=14040242
(0) [0x000000007c0e] 07c0:000e (unk. ctxt): mov dx, 0x1004  ; ba0410
<bochs:8> n
Next at t=14040243
(0) [0x000000007c11] 07c0:0011 (unk. ctxt): mov bx, 0x000c  ; bb0c00
<bochs:9> n
Next at t=14040244
(0) [0x000000007c14] 07c0:0014 (unk. ctxt): mov bp, 0x001e  ; bd1e00
<bochs:10> n
Next at t=14040245
(0) [0x000000007c17] 07c0:0017 (unk. ctxt): mov ax, 0x1301  ; b80113
<bochs:11> n
Next at t=14040246
(0) [0x000000007c1a] 07c0:001a (unk. ctxt): int 0x10  ; cd10
<bochs:12> n
Next at t=14053700
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:13> n
Next at t=14053701
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:14> n
Next at t=14053702
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:15> n
Next at t=14053703
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:16> n
Next at t=14053704
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:17> n
Next at t=14053705
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:18> n
Next at t=14053706
(0) [0x000000007c1c] 07c0:001c (unk. ctxt): jmp .-2 (0x00007c1c)  ; ebfe
<bochs:19>
```

看到如下界面

![](./images/boot.png)