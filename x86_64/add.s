.section .data
.section .text
.global _start
_start:
movq $1,%rax
movq $2,%rbx
push %rax
push %rbx
call add_a_b
add $16,%rsp
movq %rax,%rbx
movq $1,%rax
int $0x80
.type add_a_b, @function
add_a_b:
push %rbp
movq %rsp,%rbp
movq 16(%rbp),%rax
movq 24(%rbp),%rbx
add %rbx,%rax
movq %rbp,%rsp
pop %rbp
ret