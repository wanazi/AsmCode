.section .data
.section .text
.global _start
_start:
movq $6,%rax
push %rax
call adds
add $8,%rsp
movq %rax,%rbx
movq $1,%rax
int $0x80
.type adds,@function
adds:
push %rbp
movq %rsp,%rbp
sub $8,%rsp
movq 16(%rbp),%rbx
cmp $0,%rbx
je end_adds
movq %rbx,%rcx
movq %rbx,-8(%rbp)
sub $1,%rcx
push %rcx
call adds
add $8,%rsp
movq -8(%rbp),%rbx
add %rax,%rbx
end_adds:
movq %rbx,%rax
movq %rbp,%rsp
pop %rbp
ret
