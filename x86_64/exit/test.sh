#!/bin/bash
if [ ! -f exit ]; then
    echo "Binary file not found!" >&2
    exit 1
fi
chmod u+x exit
./exit
echo $?