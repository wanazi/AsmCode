#!/bin/bash

function compile(){
    local name=$1
    if [ -f ${name} ];then
        rm ${name}
    fi
    as ${name}.s -o ${name}.o
    ld ${name}.o -o ${name}
}
function clean(){
    local name=$1
    rm ${name}.o
    rm $name
}

source_files=$(ls | grep *.s | xargs -I {} basename -s .s {})

option=$1
case $option in
    compile)
        for source_file in "${source_files}"
        do
            compile $source_file
        done
    ;;
    clean)
         for source_file in "${source_files}"
        do
            clean $source_file
        done       
    ;;
    *)
        echo "Invalid option: ${option}" >&2
    ;;
esac
