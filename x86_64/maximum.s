.section .data #数据段开始
data_items: # data_items是一个地址标签，
.long 1,2,3,4,5,6,7,12,22,3,99,33,123,100,0 # long表示每个数据项占4个字节，0表示数据项的结尾
.section .text # 文本段开始
.global _start 
_start:
movl $0,%edi  # edi寄存器保存数组的索引，从0开始
movl data_items(,%edi,4),%eax # 将数组当前的值移动到eax寄存器
movl %eax ,%ebx # 由于是第一次，所以eax的值直接保存到ebx寄存器中，最为最大值
start_loop: #开始循环
cmpl $0,%eax  # eax中的值与0对比
je loop_exit  # 如果是0的话，表示数据项结束
incl %edi # edi寄存器中的值自增
movl data_items(,%edi,4),%eax # 读取数据项中的当前4个字节到eax寄存器
cmpl %ebx,%eax # 比较ebx与eax寄存器中的值大小关系
jle start_loop # 如果eax中的值小于等于ebx中的值，则继续循环
movl %eax ,%ebx # 将eax中的值保存到ebx中
jmp start_loop # 继续循环
loop_exit:
movl $1,%eax
int $0x80