.section .data
.section .text
.equ ARGS_COUNT,0
.equ PROC_NAME,4
.equ ARGS_VALUE_START,8
.global _start
_start:
movl %esp,%ebp
movl ARGS_COUNT(%esp),%ecx
cmpl $1,%ecx
jle end_proc

movl ARGS_VALUE_START(%ebp),%ebx
pushl %ebx
call sizeof_str
addl $4,%esp
movl %eax,%ebx
end_proc:
movl $1,%eax
int $0x80
.type sizeof_str,@function
sizeof_str:
pushl %ebp
movl %esp,%ebp
movl 8(%ebp),%ebx
movl $0,%edi
movl $0,%eax
count_loop:
movb (%ebx,%edi,1),%dl
cmpb $0,%dl
je exit_loop
addl $1,%eax
incl %edi
jmp count_loop
exit_loop:
movl %ebp,%esp
popl %ebp
ret


