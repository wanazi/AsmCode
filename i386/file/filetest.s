# file test
.equ LINUX_SYSCALL,0x80
.equ SYS_OPEN,5
.equ SYS_WRITE,4
.equ SYS_READ,3
.equ SYS_CLOSE,6
.equ SYS_EXIT,1

.equ O_RDONLY,0
.equ O_CREAT_WRONLY_TRUNC,03101

.equ STDIN,0
.equ STDOUT,1
.equ STDERR,2
.equ END_OF_FILE,0
.equ NUMBER_ARGUMENTS,2
.section .data
.section .bss
.equ BUFFER_SIZE,500
.lcomm BUFFER_DATA,BUFFER_SIZE
.section .text

.equ ST_ARGU_COUNT,0
.equ ST_FILE_IN,8
.equ ST_FILE_OUT,12
.equ ST_RESERVED_SPACE_SIZE,8
.equ ST_STACK_FILE_IN,-4
.equ ST_STACK_FILE_OUT,-8
.global _start
_start:
movl %esp,%ebp
subl $ST_RESERVED_SPACE_SIZE,%esp
open_files:
open_file_in:
movl $SYS_OPEN,%eax
movl ST_FILE_IN(%ebp),%ebx
movl $O_RDONLY,%ecx
movl $0666,%edx
int $LINUX_SYSCALL
store_file_in:
movl %eax,ST_STACK_FILE_IN(%ebp)
open_file_out:
movl $SYS_OPEN,%eax
movl ST_FILE_OUT(%ebp),%ebx
movl $O_CREAT_WRONLY_TRUNC,%ecx
movl $0666,%edx
int $LINUX_SYSCALL
store_file_out:
movl %eax,ST_STACK_FILE_OUT(%ebp)
read_loop_begin:
movl $SYS_READ,%eax
movl ST_STACK_FILE_IN(%ebp),%ebx
movl $BUFFER_DATA,%ecx
movl $BUFFER_SIZE,%edx
int $LINUX_SYSCALL
cmpl $END_OF_FILE,%eax
jle end_loop
continue_read_loop:
pushl $BUFFER_DATA 
pushl %eax
call toUpperCase
popl %eax
addl $4,%esp
# write to out file
movl %eax,%edx
movl $SYS_WRITE,%eax
movl ST_STACK_FILE_OUT(%ebp),%ebx
movl $BUFFER_DATA,%ecx
int $LINUX_SYSCALL

jmp read_loop_begin
end_loop:

movl $SYS_CLOSE, %eax
movl ST_STACK_FILE_OUT(%ebp),%ebx
int $LINUX_SYSCALL

movl $SYS_CLOSE, %eax
movl ST_STACK_FILE_IN(%ebp),%ebx
int $LINUX_SYSCALL

movl $1,%eax
movl $0,%ebx
int $LINUX_SYSCALL

.type toUpperCase,@function
.equ LOWERCASE_A,'a'
.equ LOWERCASE_Z,'z'
.equ UPPER_CONVERSION,'A'-'a'
toUpperCase:
pushl %ebp
movl %esp,%ebp
movl 12(%ebp),%eax
movl 8(%ebp),%ebx
movl $0,%edi
cmpl $0,%ebx
je end_convert_loop
convert_loop:
movb (%eax,%edi,1),%cl
cmpb $LOWERCASE_A,%cl
jl next_byte
cmpb $LOWERCASE_Z,%cl
jg next_byte
addb $UPPER_CONVERSION,%cl
movb %cl,(%eax,%edi,1)
next_byte:
incl %edi
cmpl %edi,%ebx
jne convert_loop
end_convert_loop:
movl %ebp,%esp
popl %ebp
ret

